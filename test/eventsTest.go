package main

import (
	"fmt"
)

func StoppedNotStarted() {
	progName := "StoppedNotStarted"
	description := "Test de l'etat STOPPED not yet started (AutoStart=false)"
	Expected := NewResult("Status: STOPPED", "", 0, CMD+"Debug")
	command := PROGPATH + " --Debug dataset/StoppedNotStarted.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "Status") == "STOPPED" &&
			GetField(SB.Rstdout, "Uptime") == "0" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func Starting() {
	progName := "Starting"
	description := "Test de l'etat STARTING (AutoStart=true, Starttime assez long pour avoir le temps de tester"
	Expected := NewResult("Status: STARTING", "", 0, CMD+"Debug")
	command := PROGPATH + " --Debug dataset/Starting.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "Status") == "STARTING" &&
			GetField(SB.Rstdout, "Pid") != "0" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func Running() {
	progName := "Running"
	description := "Test de l'etat Running (AutoStart=true, Starttime=0"
	Expected := NewResult("Status: RUNNING", "", 0, CMD+"Debug")
	command := PROGPATH + " --Debug dataset/Running.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "Status") == "RUNNING" &&
			GetField(SB.Rstdout, "Pid") != "0" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func Exited() {
	progName := "Exited"
	description := "Test de l'etat Exited (AutoStart=true, Starttime=0"
	Expected := NewResult("Status: EXITED", "", 0, CMD+"Debug")
	command := PROGPATH + " --Debug dataset/Exited.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "Status") == "EXITED" &&
			GetField(SB.Rstdout, "Pid") != "0" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func Fatal() {
	progName := "Fatal"
	description := "Test de l'etat Fatal (AutoStart=true, Starttime=10, Startretrie=1"
	Expected := NewResult("Status: FATAL", "", 0, CMD+"Debug")
	command := PROGPATH + " --Debug dataset/Fatal.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "Status") == "FATAL" &&
			GetField(SB.Rstdout, "Pid") != "0" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

// func Stopping() {
// 	progName := "Stopping"
// 	description := "Test de l'etat Stopping (AutoStart=true, Starttime=0, Stoptime=20"
// 	Expected := NewResult("Status: STOPPING", "", 0, "Stopping")
// 	command := PROGPATH + " --Debug dataset/Stopping.json"
// 	testOverSupervisor := func(SB Result, Expected Result) bool {
// 		if GetField(SB.Rstdout, "Status") == "STOPPING" &&
// 			GetField(SB.Rstdout, "Pid") != "0" {
// 			return true
// 		} else {
// 			return false
// 		}
// 	}
// 	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
// }

func Stopped() {
	progName := "Stopped"
	description := "Test de l'etat Stopped (AutoStart=true, Starttime=0, Stoptime=0"
	Expected := NewResult("Status: STOPPED", "", 0, CMD+"Stopped")
	command := PROGPATH + " --Debug dataset/Stopped.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "Status") == "STOPPED" &&
			GetField(SB.Rstdout, "Pid") != "0" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func EventsTest(title string) {
	fmt.Printf(title + ":           ")
	StoppedNotStarted()
	Starting()
	Running()
	Exited()
	Fatal()
	Stopped()
	fmt.Printf("\n")
}
