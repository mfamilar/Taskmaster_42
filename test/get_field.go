package main

import (
	"42/libft"
	// "fmt"
	"strings"
)

func GetField(output []byte, field string) string {
	job := strings.Split(string(output), "\n")
	for _, v := range job {
		split := strings.Split(v, ":")
		if libft.Trim(split[0]) == field {
			return libft.Trim(split[1])
		}
	}
	return "not found"
}
