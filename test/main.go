package main

import (
	"fmt"
	"os"
)

const (
	RED    = "\033[1;91m"
	GREEN  = "\033[1;92m"
	YELLOW = "\033[1;93m"
	BLUE   = "\033[1;94m"
	NONE   = "\033[0m"
)

var totalTest, successedTest, failedTest int

// Requirement:
// pwd set into $PATH
// progfile compiled (./taskmaster)
func main() {
	logfile, _ := os.Create(LOGFILE)
	fmt.Fprintf(logfile, YELLOW+"Taskmaster Unit Test Logger\n\n"+NONE)
	fmt.Printf(YELLOW + "Taskmaster Unit Test\n\n" + NONE)

	ArgsTest("arguments")
	InitialisationTest("initialisation")
	BuiltinStartTest("builtin_start")
	BuiltinStopTest("builtin_stop")
	BuiltinRestartTest("builtin_restart")
	EventsTest("events")
	fmt.Printf("\n-------------------------------------\n")
	fmt.Printf("Total: %d - Successed: "+GREEN+"%d"+NONE+" - Failed: "+RED+"%d"+NONE+"\n", totalTest, successedTest, failedTest)
}
