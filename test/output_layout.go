package main

import "fmt"

func BadResult() {
	fmt.Printf(RED + ". " + NONE)
}

func GoodResult() {
	fmt.Printf(GREEN + ". " + NONE)
}

func getStatus(result bool) string {
	var status string

	totalTest++
	if result == true {
		GoodResult()
		successedTest++
		status = GREEN + "Success" + NONE
	} else {
		BadResult()
		failedTest++
		status = RED + "Failed" + NONE
	}
	return status
}
