package main

import "fmt"

// Forcer la valeur de retour a 1 si erreur et que l'on quitte

func StartWithoutArgs() {
	progName := "StartWithoutArgs"
	description := "Test de la builtin start sans parametre"
	Expected := NewResult("", "usage printed", 1, CMD+"StartWithoutArgs")
	command := PROGPATH + " --Debug dataset/config2.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StartWithOneRightName() {
	progName := "StartWithOneRightName"
	description := "Test de la builtin start avec un parametre valide"
	Expected := NewResult("defaultAutoStartFalse: started\n", "", 0, CMD+"defaultAutoStartFalse")
	command := PROGPATH + " --Debug dataset/defaultAutoStartFalse.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StartWithThreeRightName() {
	progName := "StartWithThreeRightName"
	description := "Test de la builtin start avec 3 parametres valides"
	Expected := NewResult("command1: started\ncommand2: started\ncommand3: started\n", "", 0, CMD+"defaultAutoStartFalsex3")
	command := PROGPATH + " --Debug dataset/defaultAutoStartFalsex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StartWithOneBadNamme() {
	progName := "StartWithOneBadNamme"
	description := "Test de la builtin start avec 1 parametre non valide"
	Expected := NewResult("", "command42: Error (no such process)\n", 0, CMD+"StartWithOneBadName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartFalse.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StartAll() {
	progName := "StartAll"
	description := "Test de la builtin start avec le parametre all"
	Expected := NewResult("command1: started\ncommand2: started\ncommand3: started\n", "", 0, CMD+"StartAll")
	command := PROGPATH + " --Debug dataset/defaultAutoStartFalsex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StartWithBothBadAndRightName() {
	progName := "StartWithBothBadAndRightName"
	description := "Test de la builtin start des noms valides et invalides"
	Expected := NewResult("command1: started\ncommand2: started\n", "command42: Error (no such process)\ncommand4: Error (no such process)\n", 0, CMD+"StartWithBothBadAndRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartFalsex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func BuiltinStartTest(title string) {
	fmt.Printf(title + ":    ")
	StartWithoutArgs()
	StartWithOneRightName()
	StartWithThreeRightName()
	StartWithOneBadNamme()
	StartAll()
	StartWithBothBadAndRightName()
	fmt.Printf("\n")
}
