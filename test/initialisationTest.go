package main

import (
	"fmt"
	// "os"
	// "reflect"
)

func BadStopSignal() {
	progName := "ProgTestA"
	description := "Test la validite du champs signal"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/badStopSignal.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func EnvBadFormatted() {
	progName := "ProgTestA"
	description := "Test la validite du champs env"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/envBadFormat.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func ExitCodeSup255() {
	progName := "exitCodeSup255"
	description := "Test la validite du champs exit code"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/exitCodeSup255.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func ExitCodeInf0() {
	progName := "exitCodeInf0"
	description := "Test la validite du champs exit code"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/exitCodeInf0.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func AutoRestartBadValue() {
	progName := "ProgTestA"
	description := "Test la validite du champs autorestart"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/autorestartBadValue.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func WorkingdirInvalidPath() {
	progName := "WorkingdirInvalidPath"
	description := "Test la validite du champs Workingdir (Invalid Path)"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/WorkingdirInvalidPath.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func WorkingdirNoRight() {
	progName := "WorkingdirNoRight"
	description := "Test la validite du champs Workingdir (No Right)"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/WorkingdirNoRight.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func WorkingdirInFile() {
	progName := "WorkingdirInFile"
	description := "Test la validite du champs Workingdir (redirected in a file)"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/WorkingdirInFile.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func UmaskIntOverflow() {
	progName := "UmaskIntOverflow"
	description := "Test la validite du champs uMask (Int overflow)"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/UmaskIntOverflow.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func UmaskBadPattern1() {
	progName := "UmaskBadPattern1"
	description := "Test la validite du champs uMask (valeur contenant des char)"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/UmaskCharError.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func UmaskBadPattern2() {
	progName := "UmaskBadPattern2"
	description := "Test la validite du champs uMask (trop de signe '-')"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/UmaskDoubleSignError.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func UmaskBadPattern3() {
	progName := "UmaskBadPattern3"
	description := "Test la validite du champs uMask (valeur non octale)"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/UmaskIntGreaterThan7Error.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func InitialisationTest(title string) {
	fmt.Printf(title + ":   ")
	NoFieldTest()
	BadStopSignal()
	EnvBadFormatted()
	ExitCodeSup255()
	ExitCodeInf0()
	AutoRestartBadValue()
	WorkingdirInvalidPath()
	WorkingdirNoRight()
	WorkingdirInFile()
	UmaskIntOverflow()
	UmaskBadPattern1()
	UmaskBadPattern2()
	UmaskBadPattern3()
	fmt.Printf("\n")
}
