package main

import "fmt"

// Forcer la valeur de retour a 1 si erreur et que l'on quitte

func RestartWithoutArgs() {
	progName := "RestartWithoutArgs"
	description := "Test de la builtin restart sans parametre"
	Expected := NewResult("", "usage printed", 1, CMD+"RestartWithoutArgs")
	command := PROGPATH + " --Debug dataset/config2.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func RestartWithOneRightName() {
	progName := "RestartWithOneRightName"
	description := "Test de la builtin restart avec un parametre valide"
	Expected := NewResult("command1: stopped\ncommand1: started\n", "", 0, CMD+"RestartWithOneRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTrue.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func RestartWithThreeRightName() {
	progName := "RestartWithThreeRightName"
	description := "Test de la builtin restart avec 3 parametres valides"
	Expected := NewResult("command1: stopped\ncommand1: started\ncommand2: stopped\ncommand2: started\ncommand3: stopped\ncommand3: started\n", "", 0, CMD+"RestartWithThreeRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTruex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func RestartWithOneBadNamme() {
	progName := "RestartWithOneBadNamme"
	description := "Test de la builtin restart avec 1 parametre non valide"
	Expected := NewResult("", "command42: Error (no such process)\n", 0, CMD+"RestartWithOneBadName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTrue.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func RestartAll() {
	progName := "RestartAll"
	description := "Test de la builtin restart avec le parametre all"
	Expected := NewResult("command1: stopped\ncommand1: started\ncommand2: stopped\ncommand2: started\ncommand3: stopped\ncommand3: started\n", "", 0, CMD+"RestartAll")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTruex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func RestartWithBothBadAndRightName() {
	progName := "RestartWithBothBadAndRightName"
	description := "Test de la builtin restart des noms valides et invalides"
	Expected := NewResult("command1: stopped\ncommand1: started\ncommand2: stopped\ncommand2: started\n", "command42: Error (no such process)\ncommand4: Error (no such process)\n", 0, CMD+"RestartWithBothBadAndRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTruex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func BuiltinRestartTest(title string) {
	fmt.Printf(title + ":  ")
	RestartWithoutArgs()
	RestartWithOneRightName()
	RestartWithThreeRightName()
	RestartWithOneBadNamme()
	RestartAll()
	RestartWithBothBadAndRightName()
	fmt.Printf("\n")
}
