package main

import (
	"42/libft"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"syscall"
)

const (
	SBSTDOUT = "/tmp/sandbox.stdout"
	SBSTDERR = "/tmp/sandbox.stderr"
	LOGFILE  = "/tmp/taskmaster.test.log"
	PROGPATH = "taskmaster"
	CMD      = "test/command/"
)

type Result struct {
	Rstdout []byte
	Rstderr []byte
	Rexit   int
	Rstdin  string
}

func NewResult(Rstdout string, Rstderr string, Rexit int, Rstdin string) Result {
	var R Result

	R.Rstdout = []byte(Rstdout)
	R.Rstderr = []byte(Rstderr)
	R.Rexit = Rexit
	R.Rstdin = Rstdin
	return R
}

func (R Result) ShowFd(fd *os.File) {
	fmt.Fprintf(fd, "stdout: '%s'\n", R.Rstdout)
	fmt.Fprintf(fd, "stderr: '%s'\n", R.Rstderr)
	fmt.Fprintf(fd, "exit: %d\n", R.Rexit)
}

func listenerProg(progname string) (Prog Result) {
	Prog.Rstdout, _ = ioutil.ReadFile("/tmp/." + progname + ".stdout")
	Prog.Rstderr, _ = ioutil.ReadFile("/tmp/." + progname + ".stderr")
	return Prog
}

func SandBoxWithStdinUpdated(command string, stdin string) (SB Result) {
	bin, args := libft.CmdParser(command)
	cmd := exec.Command(bin, args...)
	cmd.Dir = "/tmp"

	cmd.Stdin, _ = os.OpenFile(stdin, os.O_RDONLY, 0666)
	cmd.Stdout, _ = os.Create(SBSTDOUT)
	cmd.Stderr, _ = os.Create(SBSTDERR)
	cmd.Start()
	cmd.Wait()
	SB.Rstdout, _ = ioutil.ReadFile(SBSTDOUT)
	SB.Rstderr, _ = ioutil.ReadFile(SBSTDERR)
	SB.Rexit = cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus()
	return SB
}

// return stdout, stderr, exitstatus
func SandBox(command string) (SB Result) {
	bin, args := libft.CmdParser(command)
	cmd := exec.Command(bin, args...)
	cmd.Dir = "/tmp"

	cmd.Stdout, _ = os.Create(SBSTDOUT)
	cmd.Stderr, _ = os.Create(SBSTDERR)
	cmd.Start()
	cmd.Wait()
	SB.Rstdout, _ = ioutil.ReadFile(SBSTDOUT)
	SB.Rstderr, _ = ioutil.ReadFile(SBSTDERR)
	SB.Rexit = cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus()
	return SB
}

func LogEvent(command string, status string, SB Result, Expected Result, description string) {
	logfile, _ := os.OpenFile(LOGFILE, os.O_RDWR|os.O_APPEND, 0666)

	fmt.Fprintf(logfile, "\n")
	fmt.Fprintf(logfile, YELLOW+"Command: '%s'\n"+NONE, command)
	fmt.Fprintf(logfile, "Description: "+description+"\n")
	fmt.Fprintf(logfile, "Status: %s\n\n", status)
	fmt.Fprintf(logfile, BLUE+"[Sandbox]\n"+NONE)
	SB.ShowFd(logfile)
	fmt.Fprintf(logfile, BLUE+"\n[Expected]\n"+NONE)
	Expected.ShowFd(logfile)
	fmt.Fprintf(logfile, "\n-------------------------\n")
}

func AddTest(command string, Expected Result, progName string, testOverSupervisor func(Result, Result) bool, testOverSupervisedProg func(Result, Result) bool, description string) {
	var SB Result
	var supervisedProg Result

	if Expected.Rstdin != "" {
		SB = SandBoxWithStdinUpdated(command, Expected.Rstdin)
	} else {
		SB = SandBox(command)
	}
	if testOverSupervisor != nil {
		result := testOverSupervisor(SB, Expected)
		status := getStatus(result)
		LogEvent(command, status, SB, Expected, description)
	}
	if testOverSupervisedProg != nil {
		supervisedProg.Rstdout, _ = ioutil.ReadFile("/tmp/." + progName + ".stdout")
		supervisedProg.Rstderr, _ = ioutil.ReadFile("/tmp/." + progName + ".stderr")
		result := testOverSupervisedProg(supervisedProg, Expected)
		status := getStatus(result)
		LogEvent(command, status, supervisedProg, Expected, description)
	}
}
