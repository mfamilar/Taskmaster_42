package main

import "fmt"

// launches taskmaster without any arguments (./taskmaster)
func NoArg() {
	progName := "LaunchWithoutArgs"
	description := "Test que taskmaster ne se lance pas si aucun parametre ne lui est passe"
	Expected := NewResult("", "taskmaster: usage ./taskmaster [--Debug] initFile\n", 1, "")
	command := PROGPATH
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

// launches taskmaster with too many arguments (./taskmaster arg1 arg2)
func TooManyArg() {
	progName := "LaunchWithTooManyArgs"
	description := "Test que taskmaster ne se lance pas si trop de parametres lui sont passes"
	Expected := NewResult("", "taskmaster: usage ./taskmaster [--Debug] initFile\n", 1, "")
	command := PROGPATH + " arg1 arg2"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

// launches taskmaster with an file that does not exist (./taskmaster FileNoExistIHope)
func FileNoExist() {
	progName := "LaunchWithBadFileConf"
	description := "Test que taskmaster ne se lance pas si le fichier passe en parametre n'existe pas"
	Expected := NewResult("", "taskmaster: ENOENT type error\n", 1, "")
	command := PROGPATH + " FileNoExistIHope"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

// launches taskmaster with an file with no perm (./taskmaster /test/utils/FileNoPerm)
func FileNoPerm() {
	progName := "LaunchWithBadFileConf"
	description := "Test que taskmaster ne se lance pas s'il n'a pas les bon droits sur le fichier passe en parametre"
	Expected := NewResult("", "taskmaster: EACCES type error\n", 1, "")
	command := PROGPATH + " test/utils/FileNoPerm"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

// launches taskmaster without an .Json file (./taskmaster /test/utils/FileBadFormat)
func FileBadFormat() {
	progName := "LaunchWithBadFileConf"
	description := "Test que taskmaster ne se lance pas si le fichier passe en parametre n'est pas au format json"
	Expected := NewResult("", "taskmaster: EBADF type error\n", 1, "")
	command := PROGPATH + " test/utils/FileBadFormat"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func ArgsTest(title string) {
	fmt.Printf(title + ":        ")
	NoArg()
	TooManyArg()
	FileNoExist()
	FileNoPerm()
	FileBadFormat()
	fmt.Printf("\n")
}
