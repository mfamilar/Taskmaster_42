package main

import "fmt"

// Forcer la valeur de retour a 1 si erreur et que l'on quitte

func StopWithoutArgs() {
	progName := "StoptWithoutArgs"
	description := "Test de la builtin stop sans parametre"
	Expected := NewResult("", "usage printed", 1, CMD+"StopWithoutArgs")
	command := PROGPATH + " --Debug dataset/config2.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StopWithOneRightName() {
	progName := "StopWithOneRightName"
	description := "Test de la builtin stop avec un parametre valide"
	Expected := NewResult("command1: stopped\n", "", 0, CMD+"StopWithOneRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTrue.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StopWithThreeRightName() {
	progName := "StopWithThreeRightName"
	description := "Test de la builtin stop avec 3 parametres valides"
	Expected := NewResult("command1: stopped\ncommand2: stopped\ncommand3: stopped\n", "", 0, CMD+"StopWithThreeRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTruex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StopWithOneBadNamme() {
	progName := "StopWithOneBadNamme"
	description := "Test de la builtin stop avec 1 parametre non valide"
	Expected := NewResult("", "command42: Error (no such process)\n", 0, CMD+"StopWithOneBadName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTrue.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StopAll() {
	progName := "StopAll"
	description := "Test de la builtin stop avec le parametre all"
	Expected := NewResult("command1: stopped\ncommand2: stopped\ncommand3: stopped\n", "", 0, CMD+"StopAll")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTruex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func StopWithBothBadAndRightName() {
	progName := "StopWithBothBadAndRightName"
	description := "Test de la builtin stop des noms valides et invalides"
	Expected := NewResult("command1: stopped\ncommand2: stopped\n", "command42: Error (no such process)\ncommand4: Error (no such process)\n", 0, CMD+"StopWithBothBadAndRightName")
	command := PROGPATH + " --Debug dataset/defaultAutoStartTruex3.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsResultEqual(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func BuiltinStopTest(title string) {
	fmt.Printf(title + ":     ")
	StopWithoutArgs()
	StopWithOneRightName()
	StopWithThreeRightName()
	StopWithOneBadNamme()
	StopAll()
	StopWithBothBadAndRightName()
	fmt.Printf("\n")
}
