package main

import (
// "os"
// "reflect"
)

// Test avec un fichier de configuration sans le champs signal
// Le signal envoye par defaut est "TERM"
func NoFieldSignal() {
	progName := "NoFieldSignal"
	description := "Test que le signal par defaut (SIGTERM) est applique lorsque le champs signal n'est pas present dans le fichier de configuration"
	Expected := NewResult("SIGTERM catched\n", "", 0, CMD+"command2")
	command := PROGPATH + " dataset/NoFieldSignal.json"
	testOverSupervisedProg := func(Prog Result, Expected Result) bool {
		if IsResultEqual(Prog, Expected) == true {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, nil, testOverSupervisedProg, description)
}

func NoFieldAutorestart() {
	progName := "NoFieldAutorestart"
	description := "Test que l'autorestart par defaut (unexpected) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("AutoRestart: unexpected", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldAutorestart.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "AutoRestart") == "unexpected" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldNumprocs() {
	progName := "NoFieldNumprocs"
	description := "Test que le nombre de processus  par defaut (1) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("NbProcs: 1", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldNumprocs.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "NbProcs") == "1" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldAutostart() {
	progName := "NoFieldAutostart"
	description := "Test que l'Autostart par defaut (true) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("AutoStart: true", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldAutostart.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "AutoStart") == "true" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldStartRetries() {
	progName := "NoFieldStartRetries"
	description := "Test que la valeur de StartRetries par defaut (true) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("StartRetries: 3", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldStartRetries.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "StartRetries") == "3" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldStarttime() {
	progName := "NoFieldStarttime"
	description := "Test que la valeur de Starttime par defaut (1) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("StartTime: 1", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldStarttime.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "StartTime") == "1" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldStoptime() {
	progName := "NoFieldStoptime"
	description := "Test que la valeur de StopTime par defaut (10) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("StopTime: 1", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldStoptime.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "StopTime") == "10" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldName() {
	progName := "NoFieldName"
	description := "Test que le programme qui supervise ne se lance pas dans le cas ou l'un des programmes supervises n'a pas de field name dans ses configs"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/NoFieldName.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldCmd() {
	progName := "NoFieldCmd"
	description := "Test que le programme qui supervise ne se lance pas dans le cas ou l'un des programmes supervises n'a pas de field cmd dans ses configs"
	Expected := NewResult("", "Error", 1, "")
	command := PROGPATH + " dataset/NoFieldCmd.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if IsStderrSet(SB, Expected) {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

func NoFieldExitCodes() {
	progName := "NoFieldExitCodes"
	description := "Test que la valeur de ExitCodes par defaut ([0,2]) est applique lorsque le champs n'est pas present dans le fichier de configuration"
	Expected := NewResult("ExitCodes: [0 2]", "", 0, CMD+"command2")
	command := PROGPATH + " --Debug dataset/NoFieldExitCodes.json"
	testOverSupervisor := func(SB Result, Expected Result) bool {
		if GetField(SB.Rstdout, "ExitCodes") == "[0 2]" {
			return true
		} else {
			return false
		}
	}
	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
}

//
// func NoFieldEnv() {
// 	progName := "NoFieldEnv"
// 	description := "Test que l'Env par defaut (herite du parent) est bien applique dans le cas ou le champs Env n'est pas present dans le fichier de configuration"
// 	Expected := NewResult("", "", 0, "command2")
// 	Expected.Rstdout) = os.Environ(
// 	command := PROGPATH + " --Debug dataset/NoFieldEnv.json"
// 	testOverSupervisedProg := func(SB Result, Expected Result) bool {
// 		if reflect.DeepEqual(GetField(SB.Rstdout, "Env"), os.Environ()) {
// 			return true
// 		} else {
// 			return false
// 		}
// 	}
// 	AddTest(command, Expected, progName, testOverSupervisor, nil, description)
// }

func NoFieldTest() {
	NoFieldSignal()
	NoFieldAutorestart()
	NoFieldNumprocs()
	NoFieldAutostart()
	NoFieldStartRetries()
	NoFieldStarttime()
	NoFieldStoptime()
	NoFieldName()
	NoFieldCmd()
	NoFieldExitCodes()
}
