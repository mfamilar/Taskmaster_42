package main

import "reflect"

func IsResultEqual(A Result, B Result) bool {
	if reflect.DeepEqual(A.Rstdout, B.Rstdout) &&
		reflect.DeepEqual(A.Rstderr, B.Rstderr) &&
		A.Rexit == B.Rexit {
		return true
	} else {
		return false
	}
}
