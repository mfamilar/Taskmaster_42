NAME = taskmaster
CC = go

GREEN = "\033[1;92m"
NONE = "\033[0m"

$(shell mkdir bin 2> /dev/null)

SERVER = $(filter %.go, $(shell find src/server -type f))
CLIENT = $(filter %.go, $(shell find src/client -type f))
TEST = $(filter %.go, $(shell find test -type f))

all: $(NAME)

$(NAME): server client
	# $(CC) build -o bin/$(NAME) $(SRC)
	echo $(GREEN)"Project built."$(NONE)

server: $(SERVER)
	$(CC) build -o bin/server $(SERVER)

client: $(CLIENT)
	$(CC) build -o bin/client $(CLIENT)

clean:
	/bin/rm -f bin/server bin/client bin/test
	echo $(GREEN)"Binary deleted."$(NONE)

re: clean all
	echo $(GREEN)"Project reloaded."$(NONE)

test:
	$(shell export PATH=${PWD}/bin:${PATH})
	cp -R dataset  /tmp/
	$(CC) build -o bin/test $(TEST)
	./bin/test

.SILENT: $(NAME) clean re test client server
.PHONY: test
