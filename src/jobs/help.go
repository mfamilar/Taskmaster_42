package jobs

import (
	"fmt"
	"net"
	"strings"
	"time"
)

func Help(args []string, c net.Conn) {
	if len(args) > 1 {
		output := fmt.Sprintf("*** No help on %v\n", strings.Trim(fmt.Sprint(args), "[]"))
		c.Write([]byte(output))
	} else if len(args) == 0 {
		output := fmt.Sprintf("\ndefault commands (type help <topic>):\n")
		output += fmt.Sprintf("=====================================\n")
		output += fmt.Sprintf("%-10s %-10s %-10s %-10s %-10s %-10s\n", "status", "start", "stop", "restart", "reload", "exit\n")
		c.Write([]byte(output))
	} else {
		switch args[0] {
		case "start", "stop", "restart", "status":
			Usage(args[0], c)
			return
		case "exit":
			c.Write([]byte("exit            Exit taskmaster.\n"))
		case "reload":
			c.Write([]byte("reload          Reload the configuration file.\n"))
		default:
			c.Write([]byte("*** No help on " + args[0] + "\n"))
		}
	}
	time.Sleep(time.Duration(20) * time.Millisecond)
	c.Write([]byte("EOR"))
}
