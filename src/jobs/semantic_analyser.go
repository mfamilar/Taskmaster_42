package jobs

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func ParseVarName(str *string, i *int, replace *bool) string {
	var envVar string

	*i += 2
	for *i < len(*str) && (*str)[*i] != '}' && (*str)[*i] != '$' &&
		(*str)[*i] != '{' {
		envVar = envVar + string((*str)[*i])
		*i++
	}
	if *i < len(*str) && (*str)[*i] == '}' {
		*replace = true
	} else {
		*i = *i - 1
	}
	return envVar
}

func ParameterExpansion(str *string) {
	var envVar string
	var replace bool

	replace = false
	*str = strings.Replace(*str, "~", "${HOME}", -1)
	ret := *str
	for i := 0; i < len(*str); i++ {
		if (*str)[i] == '$' && (i+2) < len(*str) && (*str)[i+1] == '{' {
			envVar = ParseVarName(str, &i, &replace)
		}
		if replace == true {
			ret = strings.Replace(ret, "${"+envVar+"}", os.Getenv(envVar), -1)
			replace = false
		}
	}

	// if os.Args[1] == "--Debug" {
	// 	fmt.Printf("before = %s and after = %s\n", *str, ret)
	// }
	*str = ret
}

func (job *Job) errmsg_layout(msg string) error {
	var msg_head string
	var msg_tail string
	var configfile string

	if os.Args[1] == "--Debug" {
		configfile = os.Args[2]
	} else {
		configfile = os.Args[1]
	}
	pathFileConf, _ := filepath.Abs(configfile)
	msg_head += "Error: "
	msg_tail += " in section 'program: " + job.Name + "' (file: '" + pathFileConf + "')"
	err := errors.New("'" + job.Name + "' a critical error has occurred")
	if len(job.Processes) > 0 {
		job.Processes[0].UpdateAndLog(Levels["critical"], events["spawnerr"], states["unknown"], err)
	} else {
		noProcess := Process{}
		noProcess.Channel = job.Channel
		noProcess.UpdateAndLog(Levels["critical"], events["spawnerr"], states["unknown"], err)
	}
	time.Sleep(1 * time.Millisecond) // Spam touche return tests
	return errors.New(msg_head + msg + msg_tail)
}

func CheckFileIsOk(job *Job, path string) error {
	var ret os.FileInfo
	var err error

	ret, err = os.Stat(path)
	if os.IsNotExist(err) == true {
		if _, err = os.Create(path); err != nil {
			return job.errmsg_layout("No such file or directory '" + path + "'")
		}
		err := errors.New("taskmaster performs updates to lunch program '" + job.Name + "'")
		job.Processes[0].UpdateAndLog(Levels["error"], events["spawnerr"], job.Processes[0].Status, err)
		return nil
	}
	if ret.Mode().IsRegular() == false {
		return job.errmsg_layout("'" + path + "' is not a file")
	}
	_, err = ioutil.ReadFile(path)
	if os.IsPermission(err) == true {
		return job.errmsg_layout("Permission denied '" + path + "'")
	}
	return nil
}

func CheckDirIsOk(job *Job, path string) error {
	var ret os.FileInfo
	var err error

	ret, err = os.Stat(path)
	if os.IsNotExist(err) == true {
		return job.errmsg_layout("No such file or directory '" + path + "'")
	}
	if ret.IsDir() == false {
		return job.errmsg_layout("'" + path + "' is not a directory")
	}
	_, err = ioutil.ReadDir(path)
	if os.IsPermission(err) == true {
		return job.errmsg_layout("Permission denied '" + path + "'")
	}
	return nil
}

func Analyser(job *Job) error {
	var found bool

	found = false

	//uMask
	if job.Umask != "" {
		ParameterExpansion(&job.Umask)
		_, err := strconv.ParseInt(job.Umask, 8, 32)
		if err != nil {
			return job.errmsg_layout(job.Umask + " can not be converted to an octal type")
		}
	}

	// STDOUT
	if job.Stdout != "" {
		ParameterExpansion(&job.Stdout)
		if ret := CheckFileIsOk(job, job.Stdout); ret != nil {
			return ret
		}
	}

	// STDERR
	if job.Stderr != "" {
		ParameterExpansion(&job.Stderr)
		if ret := CheckFileIsOk(job, job.Stderr); ret != nil {
			return ret
		}
	}

	// WORKING DIR
	if job.Workingdir != "" {
		ParameterExpansion(&job.Workingdir)
		if ret := CheckDirIsOk(job, job.Workingdir); ret != nil {
			return ret
		}
	}

	// AUTORESTART
	ParameterExpansion(&job.Autorestart)
	if job.Autorestart != "true" && job.Autorestart != "false" && job.Autorestart != "unexpected" {
		return job.errmsg_layout("invalid 'autorestart' value '" + job.Autorestart + "'")
	}

	// EXIT CODES
	for _, code := range job.Exitcodes {
		if code > 255 || code < 0 {
			return job.errmsg_layout("not a valid list of exit codes: '" + strconv.Itoa(code) + "'")
		}
	}

	// SIGNALS
	ParameterExpansion(&job.Stopsignal)
	for _, sig := range signals {
		if sig.Name == job.Stopsignal {
			found = true
			break
		}
	}
	if found == false {
		return job.errmsg_layout("value '" + job.Stopsignal + "' is not a valid signal name")
	}

	// ENV
	for i, varEnv := range job.Env {
		ParameterExpansion(&varEnv)
		job.Env[i] = varEnv
		if strings.ContainsRune(varEnv, '=') == false {
			return job.errmsg_layout("Unexpected end of key/value pairs in value '" + varEnv + "'")
		}
	}

	return nil
}

func SemanticAnalyser(jtab []Job) error {

	for i, _ := range jtab {
		err := Analyser(&jtab[i])
		if err != nil {
			return err
		}
	}
	return nil
}
