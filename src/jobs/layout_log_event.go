package jobs

import (
	"libft"
	"strconv"
	"time"
)

func (proc *Process) statusAnalyser() string {
	for _, v := range proc.Job.Exitcodes {
		if v == proc.Exit {
			return "expected"
		}
	}
	return "not expected"
}


func (proc *Process) getEventMsg(typeEvent string) string {
	var msg string

	cmdName := libft.Trim(proc.Name)
	// cmdName = BLUE + cmdName + NO_COLOR
	switch typeEvent {
	case events["spawn"]:
		msg += events["spawn"] + " '" + cmdName + "' with pid " + strconv.Itoa(proc.Pid)
	case events["success"]:
		msg += events["success"] + " " + cmdName + " entered " + states["running"] + " state, process has stayed up for > than " + strconv.Itoa(proc.Job.Starttime) + " (startsecs)"
	case events["exit"]:
		msg += events["exit"] + " " + cmdName + " (exit status " + strconv.Itoa(proc.Exit) + "; " + proc.statusAnalyser() + ")"
	case events["abort"]:
		msg += events["abort"] + " " + cmdName + " entered " + states["fatal"] + " state, too many start retries too quickly"
	case events["spawnerr"]:
		msg += events["spawnerr"] + " " + proc.EventErr
	case events["stopSignal"]:
		msg += events["stopSignal"] + " (terminated by " + proc.Job.Stopsignal + ")"
	case events["stopKill"]:
		if proc.Status == states["stopping"] {
			msg += "killing '" + proc.Job.Cmd + "' (" + strconv.Itoa(proc.Pid) + ") with SIGKILL"
		} else if proc.Status == states["stopped"] {
			msg += events["stopKill"] + " " + cmdName + " (terminated by SIGKILL)"
		}
	case events["wait"]:
		msg += events["wait"] + " for " + cmdName + " to stop"
	}
	return msg
}

func (proc *Process) LayoutLogEvent(level string, typeEvent string) string {
	var event string

	proc.TimeEvent = time.Now()
	event += proc.TimeEvent.Format(TIMELAYOUT) + " " + level + " "
	event += proc.getEventMsg(typeEvent) + "\n"
	return event
}
