package jobs

import (
	"errors"
	"strconv"
	"strings"
)

func setName(j *Job, index int) {
	if j.Numprocs == 1 {
		j.Processes[index].Name = j.Name
	} else {
		j.Processes[index].Name = j.Name + "_" + strconv.Itoa(index)
	}
}

func Init(jArray []Job, channel chan string) {
	for k, j := range jArray {
		jArray[k].Processes = make([]Process, j.Numprocs)
		for i := 0; i < j.Numprocs; i++ {
			setName(&(jArray[k]), i)
			jArray[k].Processes[i].Status = states["stopped"]
			jArray[k].Processes[i].Channel = channel
			jArray[k].Processes[i].Job = &(jArray[k])
			jArray[k].Processes[i].NoSuchFile = false
		}
	}
}

func (j *Job) setDefaultValue() error {
	ParameterExpansion(&j.Name)
	ParameterExpansion(&j.Cmd)
	if j.Name == "" || j.Cmd == "" {
		return errors.New("No command or Program name specify in the config's file")
	}
	if strings.ContainsRune(j.Name, ' ') == true {
		return errors.New("Space characters aren't allowed in the field 'Name'")
	}
	if j.Numprocs == -999 {
		j.Numprocs = jDefault.Numprocs
	} else if j.Numprocs < 0 {
		j.Numprocs = 0
	}
	if j.Autorestart == "" {
		j.Autorestart = jDefault.Autorestart
	}
	if j.Exitcodes == nil {
		j.Exitcodes = jDefault.Exitcodes
	}
	if j.Startretries == -999 {
		j.Startretries = jDefault.Startretries
	}
	if j.Starttime == -999 {
		j.Starttime = jDefault.Starttime
	}
	if j.Stopsignal == "" {
		j.Stopsignal = jDefault.Stopsignal
	}
	if j.Stoptime == -999 {
		j.Stoptime = jDefault.Stoptime
	}
	return nil
}
