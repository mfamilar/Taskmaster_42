package jobs

import "net"

func Exit(jArray []Job, c net.Conn) {
	f := func(p *Process, sync chan int) int {
		if p.Status == states["starting"] || p.Status == states["running"] {
			p.StopProcess(c, sync)
			return 0
		}
		return 0
	}
	BrowseArgsOverProcesses(jArray, []string{"all"}, f, c)
	c.Write([]byte("QUIT"))
	// BrowseProcesses(jArray, f, sync)
	// To improve : seems not wait for all processes stop before exiting
}
