package jobs

import (
	// "fmt"
	"libft"
	"net"
	"os"
	"os/exec"
	"strconv"
	"syscall"
	"time"
)

func setEnv(job *Job, cmd *exec.Cmd) {
	env := os.Environ()
J:
	for _, newVar := range job.Env {
		varToAdd := libft.StrBefore(newVar, '=')
		for i, v := range env {
			varInEnv := libft.StrBefore(v, '=')
			if varToAdd == varInEnv {
				env[i] = newVar
				break J
			}
		}
		env = append(env, newVar)
	}
	cmd.Env = env
}

func setWorkingDir(job *Job, cmd *exec.Cmd) {
	if job.Workingdir != "" {
		cmd.Dir = string(job.Workingdir)
	}
}

// if stdout and stderr not set in the job conf's file then
// both are connected to the nul device os.DevNull
func setFd(job *Job, cmd *exec.Cmd) error {
	var err error

	if job.Stdout != "" {
		// cmd.Stdout, _ = os.Create(job.Stdout)
		cmd.Stdout, err = os.OpenFile(job.Stdout, os.O_RDWR, 0666)
		if err != nil {
			return err
		}
	}
	if job.Stderr != "" {
		// cmd.Stderr, _ = os.Create(job.Stderr)
		cmd.Stderr, err = os.OpenFile(job.Stderr, os.O_RDWR, 0666)
		if err != nil {
			return err
		}
	}
	return nil
}

func setUmask(job *Job, cmd *exec.Cmd) {
	if job.Umask != "" {
		mask, _ := strconv.ParseInt(job.Umask, 8, 32)
		syscall.Umask(int(mask))
	}
}

func startTimeHandler(proc *Process, timer *time.Timer) {
	for _ = range timer.C {
		if proc.Status == states["starting"] {
			proc.UpdateAndLog(Levels["info"], events["success"], states["running"], nil)
		}
	}
}

func (proc *Process) getNextAction() string {
	switch proc.Status {
	case states["starting"], states["backoff"]:
		if proc.Job.Startretries >= 0 {
			if proc.startAttempt < proc.Job.Startretries {
				proc.startAttempt += 1
				return "restart"
			} else {
				return "abort"
			}
		}
	case states["running"]:
		if proc.Job.Autorestart == "true" {
			return "autorestart"
		} else if proc.Job.Autorestart == "unexpected" {
			for _, v := range proc.Job.Exitcodes {
				if v == proc.Exit {
					return "none"
				}
			}
			return "autorestart"
		} else {
			return "none"
		}
	}
	return "none"
}

func (proc *Process) setCmdExecutionContext(cmd *exec.Cmd) bool {
	setEnv(proc.Job, cmd)
	setWorkingDir(proc.Job, cmd)
	err := setFd(proc.Job, cmd)
	if err != nil {
		proc.UpdateAndLog(Levels["info"], events["spawnerr"], states["backoff"], err)
	}
	for err != nil && proc.Job.Startretries >= 0 && proc.startAttempt < proc.Job.Startretries {
		time.Sleep(time.Duration(proc.startAttempt) * time.Second)
		err := setFd(proc.Job, cmd)
		if err != nil {
			proc.UpdateAndLog(Levels["info"], events["spawnerr"], states["backoff"], err)
		}
		proc.startAttempt += 1
	}
	if proc.startAttempt > 0 && proc.startAttempt == (proc.Job.Startretries+1) {
		proc.UpdateAndLog(Levels["info"], events["abort"], states["fatal"], nil)
		return false
	}
	setUmask(proc.Job, cmd)
	return true
}

func updateJobInfoAfterStarting(proc *Process, cmd *exec.Cmd) {
	proc.Pid = cmd.Process.Pid
	proc.StartTimestamp = time.Now()
	proc.UpdateAndLog(Levels["info"], events["spawn"], states["starting"], nil)
}

func (proc *Process) UpdateAndLog(level string, event string, state string, err error) {
	if err != nil {
		proc.EventErr = err.Error()
	} else {
		proc.EventErr = ""
	}
	proc.Status = state
	logEvent := proc.LayoutLogEvent(level, event)
	proc.Channel <- logEvent
}

func (proc *Process) NextActionHandler(nextAction string, timerStart *time.Timer, c net.Conn, sync chan int) {

	switch nextAction {
	case "restart":
		if timerStart != nil {
			timerStart.Stop()
		}
		proc.Status = states["backoff"]
		proc.Exec(false, c, sync)
	case "abort":
		if timerStart != nil {
			timerStart.Stop()
		}
		proc.UpdateAndLog(Levels["info"], events["abort"], states["fatal"], nil)
		proc.startAttempt = 0
	case "autorestart":
		proc.startAttempt = 0
		proc.Exec(false, c, sync)
	}
}

// Exec starts the specified job
func (proc *Process) Exec(verbose bool, c net.Conn, sync chan int) {
	var nextAction string
	var timerStart *time.Timer
	var err error

	bin, args := libft.CmdParser(proc.Job.Cmd)
	cmd := exec.Command(bin, args...)
	state := proc.setCmdExecutionContext(cmd)
	if state == false {
		return
	}
	err = cmd.Start()
	if err != nil {
		if proc.startAttempt == 0 && verbose {
			c.Write([]byte("\r" + proc.Name + ": ERROR (spawn error)\n"))
			sync <- 1
		}
		proc.NoSuchFile = true
		proc.UpdateAndLog(Levels["info"], events["spawnerr"], states["backoff"], err)
		nextAction = proc.getNextAction()
	} else {
		if verbose {
			c.Write([]byte("\r" + proc.Name + ": started\n"))
			sync <- 1
		}
		updateJobInfoAfterStarting(proc, cmd)
		timerStart = time.NewTimer(time.Duration(proc.Job.Starttime) * time.Second)
		go startTimeHandler(proc, timerStart)
		cmd.Wait()
		proc.Exit = cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus()
		nextAction = proc.getNextAction()
		if proc.Status != states["stopped"] && proc.Status != states["stopping"] {
			proc.UpdateAndLog(Levels["info"], events["exit"], states["exited"], nil)
		}
	}
	proc.NextActionHandler(nextAction, timerStart, c, sync)
}
