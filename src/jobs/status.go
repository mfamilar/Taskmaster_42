package jobs

import (
	"fmt"
	"net"
	"strconv"
	"time"
)

const (
	HMINSEC    = "15:04:05"
	FORMATEXIT = "Jan _2 3:04 PM"
)

func (p *Process) getUptime() string {
	var clock time.Time

	uptime := time.Now().Sub(p.TimeEvent)
	clock = clock.Add(uptime)
	return clock.Format(HMINSEC)
}

func (p *Process) getInfos() string {
	var info string

	switch p.Status {
	case states["running"]:
		info += "pid " + strconv.Itoa(p.Pid) + ", uptime " + p.getUptime()
	case states["exited"]:
		info += p.TimeEvent.Format(FORMATEXIT)
	case states["backoff"], states["fatal"]:
		info += "Exited too quickly (process log may have details)"
	case states["stopped"]:
		var zeroTime time.Time

		if p.TimeEvent.Equal(zeroTime) {
			info += "Not started"
		} else {
			info += p.TimeEvent.Format(FORMATEXIT)
		}
	}
	return info
}

func (p *Process) ShowStatus(c net.Conn, isLast bool, sync chan int) {
	var output string

	output += fmt.Sprintf("%-15s\t%-10s\t%-80s\n\r", p.Name, p.Status, p.getInfos())
	// fmt.Printf("Output: %s\n", output)
	c.Write([]byte(output))
	sync <- 1
}

func ShowStatus(tab []Job, c net.Conn) {
	var isLast bool
	var launched int
	sync := make(chan int)

	for _, j := range tab {
		for _, p := range j.Processes {
			launched++
			go p.ShowStatus(c, isLast, sync)
			time.Sleep(time.Duration(10) * time.Millisecond)
		}
	}
	waitingForGoRoutineStatement(sync, launched)
	c.Write([]byte("EOR"))
}
