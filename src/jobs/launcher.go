package jobs

import (
	// "fmt"
	"net"
	"time"
)

func (proc *Process) Init() {
	var timer time.Time

	proc.Pid = 0
	proc.Status = states["stopped"]
	proc.Uptime = 0
	proc.Error = ""
	proc.StartTimestamp = timer
	proc.startAttempt = 0
	proc.Exit = 0
	proc.TimeEvent = timer
	proc.EventErr = ""
}

func AllSpotted(args []string) bool {
	for _, v := range args {
		if v == "all" {
			return true
		}
	}
	return false
}

func waitingForGoRoutineStatement(sync chan int, launched int) {
	var cpt int

	if launched == 0 {
		return
	}
	for _ = range sync {
		cpt += 1
		if cpt == launched {
			break
		}
		time.Sleep(time.Duration(30) * time.Millisecond)
	}
}

func BrowseArgsOverProcesses(jArray []Job, args []string,
	action func(*Process, chan int) int, c net.Conn) {

	var launched int
	sync := make(chan int)
	var nbJobs int
	var nbProcs int
	var launchAllProcesses bool

	nbJobs = len(jArray) - 1
	launchAllProcesses = AllSpotted(args)
	for _, v := range args {
		for i, job := range jArray {
			nbProcs = len(job.Processes) - 1
			for n, proc := range job.Processes {
				if launchAllProcesses == true {
					launched += action(&(jArray[i].Processes[n]), sync)
				} else if v == proc.Name {
					launched += action(&(jArray[i].Processes[n]), sync)
					goto nextArg
				} else if i == nbJobs && n == nbProcs {
					c.Write([]byte(v + ": Error (no such process)\n\r"))
					time.Sleep(time.Duration(10) * time.Millisecond)
				}
			}
		}
		if launchAllProcesses {
			break
		}
	nextArg:
	}
	waitingForGoRoutineStatement(sync, launched)
}

func BrowseProcesses(jArray []Job, action func(*Process)) {
	for i, job := range jArray {
		for n, _ := range job.Processes {
			action(&(jArray[i].Processes[n]))
		}
	}
}

func LaunchJobWithAutoStart(jtab []Job) {
	f := func(job Job) bool {
		if job.Autostart == true {
			return true
		} else {
			return false
		}
	}
	LaunchJob(jtab, f)
}

func LaunchJob(jtab []Job, test func(j Job) bool) {
	for k, j := range jtab {
		if (test == nil) || test(j) == true {
			for i, _ := range j.Processes {
				go LaunchProcess(&(jtab[k].Processes)[i])
				time.Sleep(time.Duration(15) * time.Millisecond)
			}
		}
	}
}

func LaunchProcess(p *Process) {
	p.Exec(false, nil, nil)
}
