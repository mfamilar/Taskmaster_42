package jobs

import (
	// "fmt"
	"net"
	"reflect"
)

// add timer field
func NeedReload(oldJob Job, newJob Job) bool {
	if oldJob.Cmd != newJob.Cmd ||
		oldJob.Numprocs != newJob.Numprocs ||
		oldJob.Umask != newJob.Umask ||
		oldJob.Workingdir != newJob.Workingdir ||
		oldJob.Stdout != newJob.Stdout ||
		oldJob.Stderr != newJob.Stderr ||
		reflect.DeepEqual(oldJob.Env, newJob.Env) == false {
		return true
	} else {
		return false
	}

}

func AreJobNotBrowsed(oldArray []Job) bool {
	for _, job := range oldArray {
		if job.browsed == false {
			return true
		}
	}
	return false
}

func Reload(oldArray *[]Job, newArray []Job, c net.Conn, endingString string) {
	var channel chan string

	// Cas particulier:
	// Si mauvaise commande alors Processes sera vide
	// du coup on a pas de channel, donc on return
	size := len(*oldArray)
	for i, jobs := range *oldArray {
		if len(jobs.Processes) > 0 {
			channel = jobs.Processes[0].Channel
			break
		} else if i == size {
			c.Write([]byte(endingString))
			return
		}
	}
	for _, newJob := range newArray {
		for i, oldJob := range *oldArray {
			if newJob.Name == oldJob.Name && ((*oldArray)[i].browsed == false) {
				toReload := NeedReload(oldJob, newJob)
				// fmt.Printf("Job: %s\n", newJob.Name)
				toStop := oldJob
				if toReload {
					(*oldArray)[i] = newJob
				}
				(*oldArray)[i].browsed = true
				if toReload {
					Stop([]Job{toStop}, []string{"all"}, c, false)
					Init([]Job{(*oldArray)[i]}, channel)
					Start([]Job{(*oldArray)[i]}, []string{"all"}, c, false)
				} else {
					(*oldArray)[i].Autostart = newJob.Autostart
					(*oldArray)[i].Autorestart = newJob.Autorestart
					(*oldArray)[i].Startretries = newJob.Startretries
					(*oldArray)[i].Starttime = newJob.Starttime
					(*oldArray)[i].Stopsignal = newJob.Stopsignal
					(*oldArray)[i].Stoptime = newJob.Stoptime
				}
				break
			} else {
				// add A Job to oldArray
				if i == size-1 {
					size++
					*oldArray = append(*oldArray, newJob)
					Init([]Job{(*oldArray)[size-1]}, channel)
					Start([]Job{(*oldArray)[size-1]}, []string{"all"}, c, false)
					(*oldArray)[size-1].browsed = true
				}
			}
		}
	}
	for AreJobNotBrowsed(*oldArray) {
		for i, oldJob := range *oldArray {
			if oldJob.browsed == false {
				Stop([]Job{oldJob}, []string{"all"}, c, false)
				// fmt.Printf("I: %d\n", i)
				(*oldArray)[i] = (*oldArray)[len(*oldArray)-1]
				*oldArray = (*oldArray)[:len(*oldArray)-1]
			}
		}
	}
	for i, _ := range *oldArray {
		(*oldArray)[i].browsed = false
	}
	c.Write([]byte(endingString))
}
