package jobs

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

var jDefault Job = Job{
	Numprocs:     1,
	Autostart:    true,
	Autorestart:  "unexpected",
	Exitcodes:    []int{0, 2},
	Startretries: 3,
	Starttime:    1,
	Stopsignal:   "TERM",
	Stoptime:     10,
}

func LoadDataFromFile(filename string, c chan string) ([]Job, error) {
	var err error
	var config []byte

	jtab := new([]Job)
	if config, err = ioutil.ReadFile(filename); err != nil {
		return nil, err
	}
	if err = json.Unmarshal(config, jtab); err != nil {
		return nil, err
	}
	for i, _ := range *jtab {
		for j, _ := range *jtab {
			if i != j && (*jtab)[i].Name == (*jtab)[j].Name {
				err := errors.New("Same name spotted in two differents fields 'Name'")
				return nil, err
			}
		}
	}
	for i, _ := range *jtab {
		(*jtab)[i].Autostart = true
		(*jtab)[i].Numprocs = -999
		(*jtab)[i].Startretries = -999
		(*jtab)[i].Starttime = -999
		(*jtab)[i].Stoptime = -999
	}
	if err = json.Unmarshal(config, jtab); err != nil {
		return nil, err
	}
	for i, _ := range *jtab {
		err := (*jtab)[i].setDefaultValue()
		if err != nil {
			return nil, err
		}
		(*jtab)[i].Channel = c
	}
	Init(*jtab, c)
	err = SemanticAnalyser(*jtab)
	if err != nil {
		return nil, err
	}
	return *jtab, nil
}
