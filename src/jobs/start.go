package jobs

import (
	"net"
	"time"
)

func Start(jArray []Job, args []string, c net.Conn, verbose bool) {
	if len(args) == 0 {
		Usage("start", c)
		return
	}
	f := func(proc *Process, sync chan int) int {
		if proc.NoSuchFile == true {
			c.Write([]byte(proc.Name + ": ERROR (no such file)\n"))
		} else if proc.Status != states["starting"] && proc.Status != states["running"] {
			go proc.Exec(true, c, sync)
			return 1
		} else {
			c.Write([]byte("\r" + proc.Name + ": ERROR (already started)\n"))
		}
		time.Sleep(time.Duration(15) * time.Millisecond)
		return 0
	}
	BrowseArgsOverProcesses(jArray, args, f, c)
	if verbose {
		c.Write([]byte("EOR"))
	}
}
