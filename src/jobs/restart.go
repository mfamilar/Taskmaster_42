package jobs

import (
	"net"
)

func Restart(jArray []Job, args []string, c net.Conn) {
	if len(args) == 0 {
		Usage("restart", c)
		return
	}
	Stop(jArray, args, c, false)
	Start(jArray, args, c, false)
	c.Write([]byte("EOR"))
}
