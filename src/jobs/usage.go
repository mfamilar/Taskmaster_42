package jobs

import (
	"fmt"
	"net"
	"strings"
	"time"
)

func Usage(cmd_name string, c net.Conn) {
	if cmd_name == "status" {
		output := fmt.Sprintf("%-8s %-15v %s\n", cmd_name, "<name>", "Get "+cmd_name+" for a single process")
		output += fmt.Sprintf("%-8s %-15s %s\n", cmd_name, "<name> <name>", "Get "+cmd_name+" for multiple named processes")
		output += fmt.Sprintf("%-8s %-15s %s\n", cmd_name, "", "Get all process status info")
		c.Write([]byte(output))
	} else {
		output := fmt.Sprintf("Error: %s requires a process name\n", cmd_name)
		output += fmt.Sprintf("%-8s %-15v %v%s %v\n", cmd_name, "<name>", strings.ToUpper(string(cmd_name[0])), cmd_name[1:], "a process")
		output += fmt.Sprintf("%-8s %-15v %v%s %v\n", cmd_name, "<name> <name>", strings.ToUpper(string(cmd_name[0])), cmd_name[1:], "multiple processes")
		output += fmt.Sprintf("%-8s %-15v %v%s %v\n", cmd_name, "all", strings.ToUpper(string(cmd_name[0])), cmd_name[1:], "all processes")
		c.Write([]byte(output))
	}
	time.Sleep(time.Duration(20) * time.Millisecond)
	c.Write([]byte("EOR"))
}
