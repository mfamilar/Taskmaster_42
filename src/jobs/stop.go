package jobs

import (
	"net"
	"syscall"
	"time"
)

func stopTimeHandler(proc *Process, timer *time.Timer) {
	for _ = range timer.C {
		syscall.Kill(proc.Pid, signals["kill"].Value)
		break
	}
}

func (proc *Process) StopProcess(c net.Conn, sync chan int) {
	var timerStop *time.Timer
	var err error
	var event string

	proc.UpdateAndLog(Levels["info"], events["wait"], states["stopping"], nil)
	for _, sig := range signals {
		if sig.Name == proc.Job.Stopsignal {
			err = syscall.Kill(proc.Pid, sig.Value)
			time.Sleep(100 * time.Millisecond)
			if proc.Exit != 0 {
				event = events["stopSignal"]
			}
			break
		}
	}
	if err != nil || event == "" {
		event = events["stopKill"]
		proc.UpdateAndLog(Levels["warn"], event, states["stopping"], nil)
		proc.UpdateAndLog(Levels["info"], events["wait"], states["stopping"], nil)
		timerStop = time.NewTimer(time.Duration(proc.Job.Stoptime) * time.Second)
		stopTimeHandler(proc, timerStop)
	}
	proc.UpdateAndLog(Levels["info"], event, states["stopped"], nil)
	c.Write([]byte(proc.Name + ": stopped\n\r"))
	time.Sleep(time.Duration(15) * time.Millisecond)
}

func Stop(jArray []Job, args []string, c net.Conn, verbose bool) {

	if len(args) == 0 {
		Usage("stop", c)
		return
	}
	f := func(proc *Process, sync chan int) int {
		if proc.Status == states["starting"] || proc.Status == states["running"] {
			proc.StopProcess(c, sync)
			return 0
		} else {
			if AllSpotted(args) == false {
				c.Write([]byte(proc.Name + ": ERROR (not running)\n"))
				time.Sleep(time.Duration(10) * time.Millisecond)
			}
		}
		return 0
	}
	BrowseArgsOverProcesses(jArray, args, f, c)
	if verbose {
		c.Write([]byte("EOR"))
	}
}
