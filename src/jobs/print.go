package jobs

import (
	"fmt"
)

func Prints(jtab []Job) {
	for _, j := range jtab {
		fmt.Printf("----------------------\n")
		j.Print()
		fmt.Printf("----------------------\n\n")
	}
}

func (j *Job) printProcesses() {
	for i, _ := range j.Processes {
		fmt.Printf("\n")
		fmt.Printf("\tName: %s\n", j.Processes[i].Name)
		fmt.Printf("\tStatus: %s\n", j.Processes[i].Status)
		fmt.Printf("\tPid: %d\n", j.Processes[i].Pid)
		fmt.Printf("\tUptime: %d\n", j.Processes[i].Uptime)
		fmt.Printf("\tError: %s\n", j.Processes[i].Error)
		fmt.Printf("\tTimestamp: %d\n", j.Processes[i].StartTimestamp)
	}
}

func (j *Job) Print() {
	fmt.Printf("Name: %s\n", j.Name)
	fmt.Printf("Cmd: %s\n", j.Cmd)
	fmt.Printf("NbProcs: %d\n", j.Numprocs)
	fmt.Printf("Umask: %s\n", j.Umask)
	fmt.Printf("WorkingDir: %s\n", j.Workingdir)
	fmt.Printf("AutoStart: %t\n", j.Autostart)
	fmt.Printf("AutoRestart: %s\n", j.Autorestart)
	fmt.Printf("ExitCodes: %v\n", j.Exitcodes)
	fmt.Printf("StartRetries: %d\n", j.Startretries)
	fmt.Printf("StartTime: %d\n", j.Starttime)
	fmt.Printf("StopSignal: %s\n", j.Stopsignal)
	fmt.Printf("StopTime: %d\n", j.Stoptime)
	fmt.Printf("Stdout: %s\n", j.Stdout)
	fmt.Printf("Stderr: %s\n", j.Stderr)
	fmt.Printf("Env: %v\n", j.Env)
	j.printProcesses()
}

