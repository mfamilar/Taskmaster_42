// Copyright 2016 jmaccion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package jobs implements simple functions to manipulate a process entity
// It wraps os to make it easier to handle processes
package jobs

import (
	// "net"
	"syscall"
	"time"
)

const (
	TIMELAYOUT = "2006-01-02 15:04:05.000"
	RED        = "\033[1;31m"
	GREEN      = "\033[1;32m"
	YELLOW     = "\033[1;33m"
	BLUE       = "\033[1;94m"
	WHITE_BG   = "\033[37;37;7m"
	RED_BG     = "\033[1;41m"
	NO_COLOR   = "\033[0m"
)

var signals = map[string]Signals{
	"term": Signals{"TERM", syscall.SIGTERM},
	"hup":  Signals{"HUP", syscall.SIGHUP},
	"int":  Signals{"INT", syscall.SIGINT},
	"quit": Signals{"QUIT", syscall.SIGQUIT},
	"kill": Signals{"KILL", syscall.SIGKILL},
	"usr1": Signals{"USR1", syscall.SIGUSR1},
	"usr2": Signals{"USR2", syscall.SIGUSR2},
}

var Levels = map[string]string{
	"critical": RED_BG + "CRIT" + NO_COLOR,
	"error":    "ERRO",
	"warn":     "WARN",
	"info":     WHITE_BG + "INFO" + NO_COLOR,
	"debug":    "DEBG",
	//	"trace":	"TRAC",
	//	"blather":	"BLAT",
}

var events = map[string]string{
	"spawn":      YELLOW + "spawned:" + NO_COLOR,
	"spawnerr":   RED + "spawnerr:" + NO_COLOR,
	"success":    GREEN + "success:" + NO_COLOR,
	"abort":      RED + "gave up:" + NO_COLOR,
	"exit":       "exited:",
	"wait":       YELLOW + "waiting" + NO_COLOR,
	"stopSignal": GREEN + "stopped softly:" + NO_COLOR,
	"stopKill":   RED + "stopped roughly:" + NO_COLOR,
}

var states = map[string]string{
	"stopped":  "STOPPED",
	"starting": "STARTING",
	"running":  "RUNNING",
	"backoff":  "BACKOFF",
	"stopping": "STOPPING",
	"exited":   "EXITED",
	"fatal":    "FATAL",
	"unknown":  "UNKNOWN",
}

type Signals struct {
	Name  string
	Value syscall.Signal
}

type Process struct {
	Name           string
	Pid            int
	Status         string
	Uptime         int64
	Error          string
	StartTimestamp time.Time
	Channel        chan string
	startAttempt   int
	Exit           int
	TimeEvent      time.Time
	EventErr       string
	NoSuchFile     bool
	Job            *Job
}

type Job struct {
	Name         string   `json:"name"`         //Default: No default; Required: yes
	Cmd          string   `json:"cmd"`          //Default: No default; Required: yes
	Numprocs     int      `json:"numprocs"`     //Default: 1; Required: no
	Umask        string   `json:"umask"`        //Default: no special umask; Required: no
	Workingdir   string   `json:"workingdir"`   //Default: No chdir; Required: no
	Autostart    bool     `json:"autostart"`    //Default: true; Required: no
	Autorestart  string   `json:"autorestart"`  //Default: unexpected; Required: no
	Exitcodes    []int    `json:"exitcodes"`    //Default: 0,2; Required: no
	Startretries int      `json:"startretries"` //Default: 3; Required: no
	Starttime    int      `json:"starttime"`    //Default: 1; Required: no
	Stopsignal   string   `json:"stopsignal"`   //Default: TERM; Required: no
	Stoptime     int      `json:"stoptime"`     //Default: 10; Required: no
	Stdout       string   `json:"stdout"`       //Default: Auto; Required: no
	Stderr       string   `json:"stderr"`       //Default: Auto; Required: no
	Env          []string `json:"env"`          //Default: no extra env; Required: No
	browsed      bool
	Channel      chan string
	// ToReload     bool
	Processes []Process
}
