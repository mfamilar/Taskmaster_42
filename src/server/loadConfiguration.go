package main

import (
	"jobs"
	"os"
)

func (shell *SHELL) NbArgsChecker() {
	if len(os.Args) == 3 {
		if os.Args[1] != "--Debug" {
			ft_FatalError("usage ./taskmaster [--Debug] initFile\n")
		} else {
			shell.debugMode = true
			shell.configfile = os.Args[2]
		}
	} else if len(os.Args) != 2 {
		ft_FatalError("usage ./taskmaster [--Debug] initFile\n")
	} else {
		shell.configfile = os.Args[1]
	}
}

func LoadConfiguration(shell *SHELL) {
	var err error
	c := make(chan string, 250)

	shell.channel = c
	shell.logfile, _ = os.OpenFile(LOGFILE, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
	go LogListener(shell, c)
	shell.ShellLog(jobs.Levels["info"], shell_events["start"])
	shell.NbArgsChecker()
	shell.jobs, err = jobs.LoadDataFromFile(shell.configfile, c)
	if err != nil {
		ft_FatalError(err)
	}
	jobs.LaunchJobWithAutoStart(shell.jobs)
}
