package main

import (
	// "golang.org/x/crypto/ssh/terminal"
	"jobs"
	"os"
)

const (
	RED  = "\033[1;31m"
	NONE = "\033[0m"
)

var shell_events = map[string]string{
	// "exit":  "exited",
	"start":     "started",
	"reloaderr": "reloaderr",
	// "mail":  "mailed",
}

type SHELL struct {
	jobs []jobs.Job
	// term       *terminal.Terminal
	// oldState   *terminal.State
	channel    chan string
	logfile    *os.File
	configfile string
	debugMode  bool
}
