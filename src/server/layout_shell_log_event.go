package main

import (
	"jobs"
	"os"
	"strconv"
	"time"
)

func (shell *SHELL) getEventMsg(event string) string {
	var msg string

	cmdName := jobs.BLUE + "Taskmaster" + jobs.NO_COLOR
	switch event {
	case shell_events["start"]:
		msg += cmdName + " " + shell_events["start"] + " with pid " + strconv.Itoa(os.Getpid())
	case shell_events["reloaderr"]:
		msg += RED + shell_events["reloaderr"] + NONE + ": impossible to reload the configuration's file"
	}
	return msg
}

func (shell *SHELL) ShellLog(level string, event string) {
	var logEvent string

	logEvent += time.Now().Format(jobs.TIMELAYOUT) + " " + level + " "
	logEvent += shell.getEventMsg(event) + "\n"
	shell.channel <- logEvent
}
