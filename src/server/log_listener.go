package main

import (
	"fmt"
	// "golang.org/x/crypto/ssh/terminal"
)

func LogListener(shell *SHELL, c chan string) {
	for event := range c {
		// if (shell.oldState != nil) {
		// 	terminal.Restore(0, shell.oldState)
		// }
		fmt.Fprintf(shell.logfile, "%s", event)
		// if (shell.term != nil) {
		// 	terminal.MakeRaw(0)
		// }
	}
}
