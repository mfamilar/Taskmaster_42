package main

import (
	"bytes"
	"jobs"
	"libft"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
	"fmt"
)

func getClientSideFileConfig(c net.Conn) {
	fileBuffer := make([]byte, 1024)
	var err error
	var currentByte int64

	file, err := os.Create(strings.TrimSpace("tempory.conf"))
	if err != nil {
		log.Fatal(err)
	}
	for {
		c.Read(fileBuffer)
		cleanedFileBuffer := bytes.Trim(fileBuffer, "\x00")
		_, err = file.WriteAt(cleanedFileBuffer, currentByte)

		currentByte += 1024
		if len(cleanedFileBuffer) < 1024 {
			break
		}
		libft.Memset(cleanedFileBuffer, '\x00')
	}
	file.Close()
}

func handleSignals(sigChan chan os.Signal, c net.Conn, shell *SHELL) {
	for {
		sig := <-sigChan
		switch sig {
		case syscall.SIGINT, syscall.SIGQUIT, syscall.SIGILL, syscall.SIGTRAP,
		syscall.SIGABRT, syscall.SIGEMT:
			fmt.Printf("taskmaster: quitting (signal received) ...\n")
			c.Write([]byte("\ntaskmaster: server quitting (signal received) ...\n"))
			jobs.Exit(shell.jobs, c)
			time.Sleep(time.Duration(100) * time.Millisecond)
			c.Close()
			os.Exit(0)
		case syscall.SIGHUP:
			newArray, err := jobs.LoadDataFromFile(shell.configfile, shell.channel)
			if err != nil {
				c.Write([]byte("taskmaster: error detected in the file's configuration\n"))
				time.Sleep(time.Duration(15) * time.Millisecond)
				c.Write([]byte("EOR"))
			} else {
				c.Write([]byte("\n"))
				jobs.Reload(&(shell.jobs), newArray, c, "EOR_SIG")
			}
		case syscall.SIGUSR1:
			return
		}
	}
}

func handleConnection(shell *SHELL, c net.Conn) {
	// log.Printf("Client %v" connected.", c.RemoteAddre()) -> Event

	buffer := make([]byte, 2048)
	sigChan := make(chan os.Signal, 1)
	//
	signal.Notify(sigChan, syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGILL,
		syscall.SIGTRAP,
		syscall.SIGABRT,
		syscall.SIGEMT,
		syscall.SIGUSR1)
	go handleSignals(sigChan, c, shell)
	for {
		time.Sleep(time.Duration(15) * time.Millisecond)
		n, _ := c.Read(buffer)
		if n != 0 {
			bin, args := libft.CmdParser(string(buffer[0:n]))
			switch bin {
			case "shutdown":
				jobs.Exit(shell.jobs, c)
				c.Close()
				os.Exit(0)
			case "exit":
				c.Close()
				syscall.Kill(syscall.Getpid(), syscall.SIGUSR1)
				return
			case "start":
				jobs.Start(shell.jobs, args, c, true)
			case "restart":
				jobs.Restart(shell.jobs, args, c)
			case "stop":
				jobs.Stop(shell.jobs, args, c, true)
			case "status":
				jobs.ShowStatus(shell.jobs, c)
			case "reload":
				getClientSideFileConfig(c)
				newArray, err := jobs.LoadDataFromFile("tempory.conf", shell.channel)
				if err != nil {
					c.Write([]byte("\rtaskmaster: error detected in the file's configuration\n"))
					time.Sleep(time.Duration(15) * time.Millisecond)
					shell.ShellLog(jobs.Levels["critical"], shell_events["reloaderr"])
					c.Write([]byte("EOR"))
				} else {
					jobs.Reload(&(shell.jobs), newArray, c, "EOR")
				}
			case "help":
				jobs.Help(args, c)
			default:
				c.Write([]byte("*** Unknown syntax: " + string(buffer[0:n]) + "\n"))
				time.Sleep(time.Duration(15) * time.Millisecond)
				c.Write([]byte("EOR"))
			}
		}
	}
}

func sighupLightHandler(sigChan1 chan os.Signal) {
	for {
		sig := <-sigChan1
		switch sig {
		case syscall.SIGINT, syscall.SIGQUIT, syscall.SIGILL,
		syscall.SIGTRAP, syscall.SIGABRT, syscall.SIGEMT, syscall.SIGHUP:
				continue
		case syscall.SIGUSR1:
			return
		}
	}
}

func main() {
	var shell SHELL

	sigChan1 := make(chan os.Signal, 1)
	signal.Notify(sigChan1, syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGILL,
		syscall.SIGTRAP,
		syscall.SIGABRT,
		syscall.SIGEMT,
		syscall.SIGUSR1)
	go sighupLightHandler(sigChan1)

	LoadConfiguration(&shell)
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	// fmt.Println("Server up and listening on port 8080") -> Event
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
		}
		syscall.Kill(syscall.Getpid(), syscall.SIGUSR1)
		time.Sleep(time.Duration(100) * time.Millisecond)
		handleConnection(&shell, conn)
	}
	// conn.Close()
}
