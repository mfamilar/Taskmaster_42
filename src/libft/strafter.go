package libft

func StrAfter(str string, pattern byte) string {
	var i int
	var after string

	for str[i] != 00 && str[i] != pattern {
		i++
	}
	for i > 0 && str[i] != 00 {
		after += string(str[i])
	}
	return after
}
