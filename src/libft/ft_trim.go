package libft

import (
	"strings"
)

func Trim(str string) (string) {
	var trim	string
	var i		int

	i = 0
	str = strings.TrimSpace(str)
	for i < len(str){

		if (i < len(str) && str[i] == ' '){
			trim += " "
			i++
			for i < len(str) && str[i] == ' '{
				i++
			}
		}
		for i < len(str) && str[i] != ' '{
			trim += string(str[i])
			i++
		}
	}
	return trim
}
