// Copyright 2016 jmaccion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package libft implements numerous tools functions
package libft

import (
	"strings"
)

func CmdParser(cmd string) (bin string, args []string) {
	cmd = Trim(cmd)
	split := strings.Split(cmd, " ")
	bin = split[0]
	args = split[1:]
	return
}
