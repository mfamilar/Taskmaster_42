package libft

func StrBefore(str string, pattern rune) string {
	var before string

	for _, c := range str {
		if c == pattern {
			return before
		}
		before += string(c)
	}
	return before
}
