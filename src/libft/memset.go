package libft

func Memset(array []byte, c byte) {
	for i := range array {
		array[i] = c
	}
}
