package main

import (
	"fmt"
	"os"
	"strings"
	"io/ioutil"
	"jobs"
	"encoding/json"
)

func checkFileIsWellFormed(filename string)  {
	var config []byte
	var err error
	jtab := new([]jobs.Job)

	if config, err = ioutil.ReadFile(filename); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if err := json.Unmarshal(config, jtab); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func getValue(str string) string {
	split := strings.Split(str, "=")
	if split[0] != str {
		return split[1]
	} else {
		return ""
	}
}

func argsAnalyser() (string, string) {
	var hostName string
	var configFile string
	var hostFound bool
	var configFound bool
	usage := "usage: ./bin/client --host=value --config=file.json"
	tooArgs := "Too many arguments."
	notEnoughArgs := "Not enough arguments."

	nbArgs := len(os.Args)
	if nbArgs > 3 {
		fmt.Printf("taskmaster: %s\n%s\n", tooArgs, usage)
		os.Exit(1)
	} else if nbArgs < 3 {
		fmt.Printf("taskmaster: %s\n%s\n", notEnoughArgs, usage)
		os.Exit(1)
	}
	for i, arg := range os.Args {
		if i > 0 {
			if strings.Contains(arg, "--host") {
				hostFound = true
				hostName = getValue(arg)
			} else if strings.Contains(arg, "--config") {
				configFound = true
				configFile = getValue(arg)
				checkFileIsWellFormed(configFile)
			} else {
				fmt.Printf("taskmaster: illegal option -- %s\n%s\n", arg, usage)
				os.Exit(1)
			}
		}
	}
	if !hostFound || !configFound {
		fmt.Printf("taskmaster: illegal option -- %s\n", usage)
		os.Exit(1)
	}
	return hostName, configFile
}
