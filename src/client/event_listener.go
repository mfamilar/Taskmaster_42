package main

import (
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"libft"
	"net"
	"strings"
	"time"
)

func eventListener(oldState *terminal.State, conn net.Conn, done chan bool) {
	var cpt int

	buffer := make([]byte, 2048)
	for {
		_, err := conn.Read(buffer)
		if err != nil {
			done <- false
			break
		} else if string(buffer) != "" {
			terminal.Restore(0, oldState)
			if strings.Contains(string(buffer), "EOR") == false &&
				strings.Contains(string(buffer), "QUIT") == false {
				fmt.Printf(string(buffer))
			}
			terminal.MakeRaw(0)
			if strings.Contains(string(buffer), "EOR_SIG") == true {
				terminal.Restore(0, oldState)
				fmt.Printf(PROMPT)
				terminal.MakeRaw(0)
			} else if strings.Contains(string(buffer), "EOR") == true {
				done <- true
			} else if string(buffer) == "QUIT" {
				done <- false
			}
			libft.Memset(buffer, '\x00')
		}
		time.Sleep(time.Duration(15) * time.Millisecond)
		cpt++
	}
}
