package main

import (
	"golang.org/x/crypto/ssh/terminal"
	"net"
)

func setConnectiontoHost(hostName string, oldState *terminal.State) net.Conn {
	portNum := "8080"

	conn, err := net.Dial("tcp", hostName+":"+portNum)
	if err != nil {
		terminal.Restore(0, oldState)
		ft_FatalError(err)
	}
	return conn
}
