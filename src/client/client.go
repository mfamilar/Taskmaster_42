package main

import (
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	PROMPT = "\r\033[1;92mtaskmaster \033[0m$> "
)

// SIGHUP, [done]
// SIGINT, [done]
// SIGQUIT, [done]
// SIGILL, [done]
// SIGTRAP, [done]
// SIGABRT, [done]
// SIGEMT, [done]
// SIGKILL, // not handled
// SIGBUS, // not handled
// SIGSEGV, // not handled
// SIGSYS, // not handled
// SIGTERM,
// SIGSTOP, // not handled
// SIGTSTP // suspended not handled
// SIGCONT // continued when suspended, not handled
// SIGTTIN // not handled
// SIGTTOU // not handled

func sigHandler(sigChan chan os.Signal, oldState *terminal.State, conn net.Conn) {
	<-sigChan
	terminal.Restore(0, oldState)
	fmt.Printf("\n\rtaskmaster: quitting (signal received) ...\n")
	conn.Write([]byte("exit"))
	time.Sleep(time.Duration(30) * time.Millisecond)
	conn.Close()
	os.Exit(0)

}

func main() {
	done := make(chan bool, 1)
	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGILL,
		syscall.SIGTRAP,
		syscall.SIGABRT,
		syscall.SIGEMT)
	hostName, configFile := argsAnalyser()
	term, oldState := updateTerminalMode()
	defer terminal.Restore(0, oldState)

	conn := setConnectiontoHost(hostName, oldState)

	go sigHandler(sigChan, oldState, conn)
	go eventListener(oldState, conn, done)
	for {
		cmd, err := term.ReadLine()
		if err != nil || cmd == "exit" {
			conn.Write([]byte("exit"))
			conn.Close()
			return
		}
		if cmd != "" {
			conn.Write([]byte(cmd))
			if cmd == "reload" {
				err := reloadHandler(configFile, conn)
				// time.Sleep(time.Duration(50) * time.Millisecond)
				if err != nil {
					conn.Write([]byte("EOR"))
				}
			}
			time.Sleep(time.Duration(10) * time.Millisecond)
			state := <-done
			if state == false {
				fmt.Printf("Connection with the server lost, quitting ...")
				time.Sleep(time.Duration(30) * time.Millisecond)
				return
			}
		}
	}
}
