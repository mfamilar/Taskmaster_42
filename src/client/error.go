package main

import "os"
import "fmt"

func ft_FatalError(data interface{}){
	ft_Error(data)
	os.Exit(1)
}

func ft_Error(data interface{}){
	
	fmt.Fprintf(os.Stderr, "taskmaster: ")
	switch data.(type){
		case error:
			fmt.Fprintf(os.Stderr, "%s\n", data)
		default:
			fmt.Fprintf(os.Stderr, "%s", data)
	}
}
