package main

import (
	"io"
	"libft"
	"net"
	"os"
	"time"
)

func reloadHandler(configFile string, conn net.Conn) error {
	time.Sleep(time.Duration(10) * time.Millisecond)
	file, err := os.Open(libft.Trim(configFile))
	if err != nil {
		// ft_FatalError(err)
		ft_Error(err)
		return err
	}
	_, err = io.Copy(conn, file)
	if err != nil {
		// ft_FatalError(err)
		ft_Error(err)
		return err
	}
	return nil
}
