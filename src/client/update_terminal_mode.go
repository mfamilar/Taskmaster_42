package main

import (
	"fmt"
	"golang.org/x/crypto/ssh/terminal"
	"os"
)

func updateTerminalMode() (*terminal.Terminal, *terminal.State) {
	if terminal.IsTerminal(int(os.Stdin.Fd())) {
		oldState, err := terminal.MakeRaw(0)
		if err != nil {
			ft_FatalError(err)
		}
		return terminal.NewTerminal(os.Stdin, PROMPT), oldState
	} else {
		fmt.Printf("taskmaster: impossible to launch a control's shell\n")
		os.Exit(1)
	}
	return nil, nil
}
