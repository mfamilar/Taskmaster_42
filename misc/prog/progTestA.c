#include <unistd.h>
#include <stdlib.h>

/*
** arg[1] => sleep time
** arg[2] => return value
*/

int		main(int ac, char **av)
{
	(void)ac;
	sleep(atoi(av[1]));
	return (atoi(av[2]));

}
