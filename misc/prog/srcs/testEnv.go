package main

import (
	"os"
	"fmt"
	"time"
)

func main() {
	timer := time.NewTimer(time.Duration(5) * time.Second)
	for _ = range timer.C {
		for _, v := range os.Environ() {
			fmt.Printf("%s\n", v)
		}
		break
	}
}
