#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

void	sigterm_handler(int sig)
{
	(void)sig;
	dprintf(STDOUT_FILENO, "SIGTERM catched\n");
	exit(42);
}

int		main(void)
{
	signal(SIGTERM, sigterm_handler);
	while (1){

	}
	return (0);
}
